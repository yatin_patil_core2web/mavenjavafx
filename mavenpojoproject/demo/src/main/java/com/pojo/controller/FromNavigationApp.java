package com.pojo.controller;

import com.pojo.dataAccess.FromPage1;
import com.pojo.dataAccess.FromPage2;
import com.pojo.dataAccess.FromPage3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FromNavigationApp extends Application{
    private Stage primaryStage;
    private Scene page1Scene,page2Scene,page3Scene;
    
    // page1 instance
    private FromPage1 page1;

    // page2 instance
    private FromPage2 page2;

    // page3 instance
    private FromPage3 page3;



    @Override
    public void start(Stage primaryStage)  {
        this.primaryStage=primaryStage;

        // Initialize pages
        page1=new FromPage1(this);
        page2=new FromPage2(this);
        page3=new FromPage3(this);

        // Create a Scnene for each page with specific dimension
        page1Scene =new Scene(page1.getView(),400,300);
        page2Scene=new Scene(page2.getView(), 400,300);
        page3Scene =new Scene(page3.getView(), 400,300);  
        
        // set the initial Scene to page1Scene
        primaryStage.setScene(page1Scene);
        primaryStage.setTitle("From Navigation");
        primaryStage.show();

        

    }
    // method for navigating to page1
    public void navigateToPage1(){
        // preserve data and navigate to page1
        page2.setField2(page2.getField2());
        page1.setField1(page1.getField1());
        primaryStage.setScene(page1Scene);

    }

    // method for navigating to page2
    public void navigateToPage2(){

        //Preserve data and navigate to page2

        page1.setField1(page1.getField1());
        page3.setField3(page3.getField3());
        primaryStage.setScene(page2Scene);
    }

     // method for navigating to page2
     public void navigateToPage3(){

        //Preserve data and navigate to page2

        page2.setField2(page2.getField2());
        
        primaryStage.setScene(page3Scene);
    }

}
