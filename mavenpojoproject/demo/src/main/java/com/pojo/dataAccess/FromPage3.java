package com.pojo.dataAccess;

import com.pojo.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;

import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FromPage3 {
    public void setApp(FromNavigationApp app) {
        this.app = app;
    }

    public void setView(GridPane view) {
        this.view = view;
    }

    public void setField3(TextField field3) {
        this.field3 = field3;
    }

    private FromNavigationApp app;
    private GridPane view;
    private TextField field3;

    public FromPage3(FromNavigationApp app){
        this.app=app;
        initialize();
    }

    public FromNavigationApp getApp() {
        return app;
    }

    public GridPane getView() {
        return view;
    }

    public TextField getField3() {
        return field3;
    }

//Initialize the layout and components
 private void initialize(){
    //create a Gridpane
    view=new GridPane();
    view.setPadding(new Insets(10));
    view.setHgap(10);
    view.setVgap(10);

    Label label3=new Label("Field3 :");

    // create a new TextFiled 
    field3=new TextField();

    // Crate a button to navigate back to the page
    Button backButton=new Button("Back");
    backButton.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent event){
            app.navigateToPage2();

        }
    });
    
   
    // create a button to clear the text field
    Button clearButton=new Button("clear");
    clearButton.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent event){

            setField3(null);
        }
    });

    // Add the component to the gridpane
    view.add(label3, 0,0);
    view.add(field3, 1, 0);
    view.add(backButton, 0, 1);
    view.add(clearButton, 2, 1);
}

 /*    // method to get the view of the page
    public GridPane getView(){

        return view;
    }

    // method to get the value of field 1
    public String getField3Value(){
        return field3.getText();


    }
    //method to set the value of field 1
    public void setField3Value(String value){

        field3.setText(value);
    }*/
    
}
