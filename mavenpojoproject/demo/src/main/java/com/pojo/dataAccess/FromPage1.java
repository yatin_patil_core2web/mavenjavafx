package com.pojo.dataAccess;


import com.pojo.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FromPage1 {

    private FromNavigationApp app;
    private GridPane view;
    private TextField field1;
public void setApp(FromNavigationApp app) {
        this.app = app;
    }

    public void setView(GridPane view) {
        this.view = view;
    }

    public void setField1(TextField field1) {
        this.field1 = field1;
    }

public FromNavigationApp getApp() {
        return app;
    }

    public GridPane getView() {
        return view;
    }

    public TextField getField1() {
        return field1;
    }

    // constructor for Frompage1
    public FromPage1(FromNavigationApp app){
        this.app=app;
        initialize();
}

 //Initialize the layout and components
 private void initialize(){
        //create a Gridpane
        view=new GridPane();
        view.setPadding(new Insets(10));
        view.setHgap(10);
        view.setVgap(10);

        Label label1=new Label("Field 1: ");
        label1.setTextFill(javafx.scene.paint.Color.RED);

        // create a new TextFiled 
    
        field1=new TextField();

        // Create a button to navigate to the next page
        Button nextButton=new Button("Next");
        nextButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                app.navigateToPage2();
            }
            
   
        });

        // create a button to clear the text field
        Button clearButton=new Button("clear");
        clearButton.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                setField1(null);
            }
            
        });

        // Add the component to the gridpane
        view.add(label1, 0,0);
        view.add(field1, 1, 0);
        view.add(nextButton, 1, 1);
        view.add(clearButton, 2, 1);
        //view.setStyle("-fx-background-image:url(images/ganapati.jpeg); -fx-background-repeat:no-repeat; -fx-background-size:250");
        view.setStyle("-fx-background-image:url(image/ganapati.jpeg); -fx-background-repeat:no-repeat; -fx-background-size:400");
      

       
    }
    
}
