package com.pojo.dataAccess;



import com.pojo.controller.FromNavigationApp;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class FromPage2 {
    private FromNavigationApp app;
    private GridPane view;
    private TextField field2;

    public FromPage2(FromNavigationApp app){
        this.app=app;
        initialize();
    }

    //Initialize the layout and components
 private void initialize(){
    //create a Gridpane
    view=new GridPane();
    view.setPadding(new Insets(10));
    view.setHgap(10);
    view.setVgap(10);

    Label label2=new Label("Field 2: ");

    // create a new TextFiled 

    field2=new TextField();

    // Crate a button to navigate back to the page
    Button backButton=new Button("Back");
    backButton.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent event){
            app.navigateToPage1();

        }
    });
    
    // crete a nextButton
    Button nextButton=new Button("Next");
    nextButton.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent event){
            app.navigateToPage3();

        }
    });
    // create a button to clear the text field
    Button clearButton=new Button("clear");
    clearButton.setOnAction(new EventHandler<ActionEvent>() {
        public void handle(ActionEvent event){

            setField2(null);
        }
    });

    // Add the component to the gridpane
    view.add(label2, 0,0);
    view.add(field2, 1, 0);
    view.add(backButton, 0, 1);
    view.add(nextButton, 1, 1);
    view.add(clearButton, 2, 1);


    
}

public FromNavigationApp getApp() {
    return app;
}

public GridPane getView() {
    return view;
}

public TextField getField2() {
    return field2;
}

public void setApp(FromNavigationApp app) {
    this.app = app;
}

public void setView(GridPane view) {
    this.view = view;
}

public void setField2(TextField field2) {
    this.field2 = field2;
}


}
