package com.pojo;

public class playersData {
    private String playername;
    private String country;
    private int age;
    // getter method for player name 
    public String getPlayername() {
        return playername;
    }

    // getter method for country
    public String getCountry() {
        return country;
    }
    // getter method for age 
    public int getAge() {
        return age;
 
    }
    // setter from the playername    


    public void setPlayername(String playername) {
        this.playername = playername;
    }

    // setter from the country
    public void setCountry(String country) {
        this.country = country;
    }

    //setter from the age 

    public void setAge(int age) {
        this.age = age;
    }

    
    
}
